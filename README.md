# CERIcompiler

**This version Can handle :**

// Program := [VarDeclarationPart] StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type
// StatementPart := Statement {";" Statement} "."
// AssignementStatement := Ident "=" Expression
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | Display
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement ( "TO" | "DOWNTO" ) Expression [ "STEP" Number ] "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Float | Ident | "(" Expression ")"| "!" Factor
// Number := Digit { Digit }
// Float := Digit { Digit } "." Digit { Digit }
// Ident := Letter { Letter | Digit }
// Char:= Letter | Digit
// Bool:= "TRUE" | "FALSE"

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
// Type := "INT" | "BOOL" | "CHAR" | "DOUBLE"

**How to use :**

Le code à compiler est à insérer dans test.p

Commande **make** pour construire le fichier

Commande **./test** pour exécuter le ficher constuit

La fonction DISPLAY du langage permet d'afficher des éléments dans le terminal

Le fichier test.s contient le code compilateur généré


-------------------------------------------------------------------------------------------------------------


Par défaut, toutes les valeurs sont initialisées à 0

Lorsqu'on affiche une variable de type BOOL, le terminal retourne 1 pour VRAI et 0 pour FAUX

La fonction FOR n'admet qu'une varaible de type INT comme itérateur et va jusqu'à la valeur indiqué compris

Les noms de variables "popst" et "forstep" sont réservés pour le fonctionnement du compilateur

Il est impossible de faire des opérations entre un INT et un DOUBLE, ainsi 30+3.0 sera invalide, mais 30.0+3.0 ou 30+3 sera valide


Exemple de syntaxe :

VAR 
	INT : a,e;
	BOOL : b;
	CHAR : c;
	DOUBLE : d.
b:=TRUE;
c:='c';
d:=3.6;
IF b THEN DISPLAY 'v';
WHILE d<4.0 DO
	BEGIN
		d:=d+0.1;
		DISPLAY d;
	END;
FOR a:=1 TO 3 DO
	BEGIN
        e:=e+2;
        DISPLAY e;
	END
.
