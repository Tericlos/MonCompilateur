			# This code was produced by the CERI Compiler
	.data
FormatString1:	.string "%llu\n"	#used by printf to display 64-bit unsigned integers
FormatString2:	.string "%f\n"		#used by printf to display 64-bit unsigned integers
FormatString3:	.string "%c\n"		#used by printf to display 64-bit unsigned integers
	.align 8
popst:	.double 0.0		#Variable permettant de pop le registre st
forstep:	.quad 0		#Variable permettant de conserver le pas de la fonction FOR
a:	.quad 0
e:	.quad 0
b:	.quad 0
c:	.byte 0
d:	.double 0.0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp		# Save the position of the stack's top
	push $0
	pop b
	push $99
	pop c
	movq $4614162998222441677, %rax
	push %rax
	pop d
IF1:
	push b
	pop %rax
	cmpq $0, %rax
	je ELSE1
THEN1:
#DISPLAY
	push $118
	pop %rsi
	movq $FormatString3, %rdi
	movb $0, %al
	subq $8, %rsp
	call printf
	addq $8, %rsp
#DISPLAYED
jmp ENDIF1
ELSE1:
ENDIF1:
WHILE3:
	push d
	movq $4616189618054758400, %rax
	push %rax
	fldl (%rsp)
	addq $8, %rsp
	fldl (%rsp)
	addq $8, %rsp
	fcomip %st(1), %st(0)
	fstpl popst
	jb VRAI4
	push $0
	jmp SUITE4
VRAI4:
	push $0xFFFFFFFFFFFFFFFF
SUITE4:
	pop %rax
	cmpq $0, %rax
	je ENDWHILE3
DO3:
BEGIN5:
	push d
	movq $4591870180066957722, %rax
	push %rax
	fldl (%rsp)
	addq $8, %rsp
	fldl (%rsp)
	addq $8, %rsp
	faddp %st(0), %st(1)
	subq $8, %rsp
	fstpl (%rsp)
	pop d
#DISPLAY
	push d
	movsd (%rsp), %xmm0
	addq $8, %rsp
	movq $FormatString2, %rdi
	movb $1, %al
	subq $8, %rsp
	call printf
	addq $8, %rsp
#DISPLAYED
END5:
	jmp WHILE3
ENDWHILE3:
FOR7:
	push $1
	pop forstep
	push $1
	pop a
TO7:
	push $3
	pop %rax
	push a
	pop %rbx
	cmpq %rax, %rbx
	ja ENDFOR7
DO7:
BEGIN8:
	push e
	push $2
	pop %rbx
	pop %rax
	addq	%rbx, %rax
	push %rax
	pop e
#DISPLAY
	push e
	pop %rsi
	movq $FormatString1, %rdi
	movb $0, %al
	subq $8, %rsp
	call printf
	addq $8, %rsp
#DISPLAYED
END8:
	push forstep
	pop %rax
	addq %rax, a
	jmp TO7
ENDFOR7:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
