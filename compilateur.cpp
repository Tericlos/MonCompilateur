//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <utility>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {INT, BOOL, CHAR, DOUBLE};

TOKEN current;

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, TYPE> StringToType {{"INT", INT}, {"BOOL", BOOL}, {"CHAR", CHAR}, {"DOUBLE", DOUBLE}};
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

string TypeToString(TYPE t){
	switch(t){
		case INT:
			return("INT");
		case BOOL:
			return("BOOL");
		case CHAR:
			return("CHAR");
		case DOUBLE:
			return("DOUBLE");
	}
}

string itob(int n)
{
    string r;
    while(n!=0) {r=(n%2==0 ?"0":"1")+r; n/=2;}
    return r;
}

void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [VarDeclarationPart] StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type
// StatementPart := Statement {";" Statement} "."
// AssignementStatement := Ident "=" Expression
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | Display
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement ( "TO" | "DOWNTO" ) Expression ["STEP" Number] "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Float | Ident | "(" Expression ")"| "!" Factor
// Number := Digit { Digit }
// Float := Digit { Digit } "." Digit { Digit }
// Ident := Letter { Letter | Digit }
// Char:= Letter | Digit
// Bool:= "TRUE" | "FALSE"

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
// Type := "INT" | "BOOL" | "CHAR" | "DOUBLE"


TYPE Identifier(){
	if(IsDeclared(lexer->YYText())){
		string ident=lexer->YYText();
		cout << "\tpush "<<lexer->YYText()<<endl;
		current=(TOKEN) lexer->yylex();
		return (DeclaredVariables[ident]);
	}
	else
		Error("cette variable n'est pas déclarée");
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return (INT);
}

TYPE Boolean(void){
	if(strcmp(lexer->YYText(),"FALSE")==0)
		cout <<"\tpush $0"<<endl;
	else if(strcmp(lexer->YYText(),"TRUE")==0)
		cout <<"\tpush $1"<<endl;
	else
		Error("mot clé TRUE ou FALSE attendu");
	current=(TOKEN) lexer->yylex();
	return (BOOL);
}

TYPE Char(void){
	string s=lexer->YYText();
	int c=s[1];
	cout <<"\tpush $"<<c<<endl;
	current=(TOKEN) lexer->yylex();
	return(CHAR);
}

TYPE Float(void){
	double d=atof(lexer->YYText());
	long long unsigned int *i;
	i=(long long unsigned int *) &d;
	cout <<"\tmovq $"<<(*i)<<", %rax"<<endl;
	cout <<"\tpush %rax"<<endl;
	current=(TOKEN) lexer->yylex();
	return (DOUBLE);
}

TYPE Expression(void);			// Called by Term() and calls Term()

// Factor := Number | Float | Ident | Char | "(" Expression ")"| "!" Factor
TYPE Factor(void){
	TYPE t;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		t=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else {
			current=(TOKEN) lexer->yylex();
			return (t);
		}
	}
	else {
		switch(current){
			case NUMBER:
				t=Number();
				return (t);
			case FLOAT:
				t=Float();
				return (t);
			case ID:
				t=Identifier();
				return (t);
			case CHARCONST:
				t=Char();
				return(t);
			default:
				Error("'(' ou chiffre ou lettre attendue");
		}
	}
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE t, cmp;
	OPMUL mulop;
	t=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();
		cmp=Factor();
		if(cmp==t){
			switch(t){
				case INT:
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					switch(mulop){
						case AND:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur AND uniquement pour les valeurs booléennes"<<endl;
							exit(-1);
						case MUL:
							cout << "\tmulq	%rbx"<<endl;
							cout << "\tpush %rax"<<endl;
							break;
						case DIV:	
							cout << "\tmovq $0, %rdx"<<endl; 
							cout << "\tdiv %rbx"<<endl;
							cout << "\tpush %rax"<<endl;
							break;
						case MOD:
							cout << "\tmovq $0, %rdx"<<endl; 
							cout << "\tdiv %rbx"<<endl;
							cout << "\tpush %rdx"<<endl;
							break;
						default:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu"<<endl;
							exit(-1);
					}
				case BOOL:
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					if(mulop==AND){
						cout << "\tmulq	%rbx"<<endl;
						cout << "\tpush %rax"<<endl;
					}
					else {
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur incompatible avec les valeurs booléennes"<<endl;
						exit(-1);
					}
				case DOUBLE:
					cout << "\tfldl (%rsp)"<<endl;
					cout << "\taddq $8, %rsp"<<endl;
					cout << "\tfldl (%rsp)"<<endl;
					cout << "\taddq $8, %rsp"<<endl;
					switch(mulop){
						case MUL:
							cout << "\tfmulp %st(0), %st(1)"<<endl;
							cout << "\tsubq $8, %rsp"<<endl;
							cout << "\tfstpl (%rsp)"<<endl;
							break;
						case DIV:
							cout << "\tfdivrp %st(1), %st(0)"<<endl;
							cout << "\tsubq $8, %rsp"<<endl;
							cout << "\tfstpl (%rsp)"<<endl;
							break;
						case MOD:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur MOD non implémenté pour les doubles"<<endl;
							exit(-1);
						case AND:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur OR uniquement pour les valeurs booléennes"<<endl;
							exit(-1);
						default:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu"<<endl;
							exit(-1);
					}
				case CHAR:
					cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération multiplicative impossible sur des variables de type CHAR"<<endl;
					exit(-1);
			}
		}
		else {
			cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération impossible entre deux valeurs de type différent"<<endl;
			exit(-1);
		}
	}
	return(t);
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE t, cmp;
	OPADD adop;
	t=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();
		cmp=Term();
		if(t==cmp){
			switch(t){
				case INT:
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					switch(adop){
						case ADD:
							cout << "\taddq	%rbx, %rax"<<endl;
							cout << "\tpush %rax"<<endl;
							break;			
						case SUB:	
							cout << "\tsubq	%rbx, %rax"<<endl;
							cout << "\tpush %rax"<<endl;
							break;
						case OR:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur OR uniquement pour les valeurs booléennes"<<endl;
							exit(-1);
						default:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu"<<endl;
							exit(-1);
					}
					break;
				case BOOL:
					cout << "\tpop %rbx"<<endl;
					cout << "\tpop %rax"<<endl;
					if(adop==OR){
						cout << "\taddq	%rbx, %rax"<<endl;
						cout << "\tpush %rax"<<endl;
					}
					else {
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur incompatible avec les valeurs booléennes"<<endl;
						exit(-1);
					}
					break;
				case DOUBLE:
					cout << "\tfldl (%rsp)"<<endl;
					cout << "\taddq $8, %rsp"<<endl;
					cout << "\tfldl (%rsp)"<<endl;
					cout << "\taddq $8, %rsp"<<endl;
					switch(adop){
						case ADD:
							cout << "\tfaddp %st(0), %st(1)"<<endl;
							cout << "\tsubq $8, %rsp"<<endl;
							cout << "\tfstpl (%rsp)"<<endl;
							break;
						case SUB:	
							cout << "\tfsubrp %st(1), %st(0)"<<endl;
							cout << "\tsubq $8, %rsp"<<endl;
							cout << "\tfstpl (%rsp)"<<endl;
							break;
						case OR:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur OR uniquement pour les valeurs booléennes"<<endl;
							exit(-1);
						default:
							cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu"<<endl;
							exit(-1);
					}
					break;
				case CHAR:
					cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération additive impossible sur des variables de type CHAR"<<endl;
					exit(-1);
			}
		}
		else {
			cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération impossible entre deux valeurs de type différent"<<endl;
			exit(-1);
		}
	}
	return(t);
}	

// VarDeclaration := Type ":" Ident {"," Ident}  
void VarDeclaration(void){
	if(current==TYP){
		TYPE t;
		if(StringToType.find(lexer->YYText())!=StringToType.end())
			t=StringToType[lexer->YYText()];
		current=(TOKEN) lexer->yylex();
		if(current==COLON){
			do {
				current=(TOKEN) lexer->yylex();
				if(current==ID){
					if(strcmp(lexer->YYText(),"popst")==0 || strcmp(lexer->YYText(),"forstep")==0){
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le nom de variable "<<lexer->YYText()<<" est reservé pour le compilateur"<<endl;
						exit(-1);
					}
					string size;
					switch(t){
						case INT:
							size=":\t.quad 0";
							break;
						case BOOL:
							size=":\t.quad 0";
							break;
						case CHAR:
							size=":\t.byte 0";
							break;
						case DOUBLE:
							size=":\t.double 0.0";
							break;
					}
					cout << lexer->YYText() << size <<endl;
					DeclaredVariables.insert(pair<string, TYPE>(lexer->YYText(), t));
					current=(TOKEN) lexer->yylex();
				}
				else
					Error("identificateur attendu");
			} while (current==COMMA);
		}
		else
			Error("':' attendu");
	}
	else
		Error("type de variable attendu");
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0){
		cout << "\t.align 8"<<endl;
		cout << "popst:\t.double 0.0\t\t#Variable permettant de pop le registre st" <<endl;
		cout << "forstep:\t.quad 0\t\t#Variable permettant de conserver le pas de la fonction FOR" <<endl;
		do {			
			current=(TOKEN) lexer->yylex();
			VarDeclaration();
		} while(current==SEMICOLON);
		if(current==DOT)
			current=(TOKEN) lexer->yylex();
		else
			Error("point attendu");
	}
	else
		Error ("VAR attendu");
}
	
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(lexer->YYText()=="=") Error ("l'opérateur de comparaison est '=='");
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE t;
	OPREL oprel;
	t=SimpleExpression();
	if(current==RELOP){
		TYPE cmp;
		oprel=RelationalOperator();
		cmp=SimpleExpression();
		if(t==cmp) {
			if(t==BOOL){
				cout << "\tpop %rax"<<endl;
				cout << "\tpop %rbx"<<endl;
				cout << "\tcmpq %rax, %rbx"<<endl;
				switch(oprel){
					case EQU:
						cout << "\tje VRAI"<<++TagNumber<<endl;
						break;
					case DIFF:
						cout << "\tjne VRAI"<<++TagNumber<<endl;
						break;
					default:
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération inférieur/supérieur impossible entre deux valeurs booléennes"<<endl;
						exit(-1);
				}
				cout << "\tpush $0"<<endl;
				cout << "\tjmp SUITE"<<TagNumber<<endl;
				cout << "VRAI"<<TagNumber<<":"<<endl;
				cout << "\tpush $0xFFFFFFFFFFFFFFFF"<<endl;	
				cout << "SUITE"<<TagNumber<<":"<<endl;
				return (BOOL);
			}
			else if(t==INT){
				cout << "\tpop %rax"<<endl;
				cout << "\tpop %rbx"<<endl;
				cout << "\tcmpq %rax, %rbx"<<endl;
				switch(oprel){
					case EQU:
						cout << "\tje VRAI"<<++TagNumber<<endl;
						break;
					case DIFF:
						cout << "\tjne VRAI"<<++TagNumber<<endl;
						break;
					case SUPE:
						cout << "\tjae VRAI"<<++TagNumber<<endl;
						break;
					case INFE:
						cout << "\tjbe VRAI"<<++TagNumber<<endl;
						break;
					case INF:
						cout << "\tjb VRAI"<<++TagNumber<<endl;
						break;
					case SUP:
						cout << "\tja VRAI"<<++TagNumber<<endl;
						break;
					default:
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu (l'opérateur de comparaison est '==')"<<endl;
						exit(-1);
				}
				cout << "\tpush $0"<<endl;
				cout << "\tjmp SUITE"<<TagNumber<<endl;
				cout << "VRAI"<<TagNumber<<":"<<endl;
				cout << "\tpush $0xFFFFFFFFFFFFFFFF"<<endl;	
				cout << "SUITE"<<TagNumber<<":"<<endl;
				return (BOOL);
			}
			else if(t==DOUBLE){
				cout << "\tfldl (%rsp)"<<endl;
				cout << "\taddq $8, %rsp"<<endl;
				cout << "\tfldl (%rsp)"<<endl;
				cout << "\taddq $8, %rsp"<<endl;
				cout << "\tfcomip %st(1), %st(0)"<<endl;
				cout << "\tfstpl popst"<<endl;
				switch(oprel){
					case EQU:
						cout << "\tje VRAI"<<++TagNumber<<endl;
						break;
					case DIFF:
						cout << "\tjne VRAI"<<++TagNumber<<endl;
						break;
					case SUPE:
						cout << "\tjae VRAI"<<++TagNumber<<endl;
						break;
					case INFE:
						cout << "\tjbe VRAI"<<++TagNumber<<endl;
						break;
					case INF:
						cout << "\tjb VRAI"<<++TagNumber<<endl;
						break;
					case SUP:
						cout << "\tja VRAI"<<++TagNumber<<endl;
						break;
					default:
						Error("Opérateur de comparaison inconnu");
				}
				cout << "\tpush $0"<<endl;
				cout << "\tjmp SUITE"<<TagNumber<<endl;
				cout << "VRAI"<<TagNumber<<":"<<endl;
				cout << "\tpush $0xFFFFFFFFFFFFFFFF"<<endl;	
				cout << "SUITE"<<TagNumber<<":"<<endl;
				return (BOOL);
			}
			else if(t==CHAR){
				cout << "\tpop %rax"<<endl;
				cout << "\tpop %rbx"<<endl;
				cout << "\tcmpb %al, %bl"<<endl;
				switch(oprel){
					case EQU:
						cout << "\tje VRAI"<<++TagNumber<<endl;
						break;
					case DIFF:
						cout << "\tjne VRAI"<<++TagNumber<<endl;
						break;
					case SUPE:
						cout << "\tjae VRAI"<<++TagNumber<<endl;
						break;
					case INFE:
						cout << "\tjbe VRAI"<<++TagNumber<<endl;
						break;
					case INF:
						cout << "\tjb VRAI"<<++TagNumber<<endl;
						break;
					case SUP:
						cout << "\tja VRAI"<<++TagNumber<<endl;
						break;
					default:
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opérateur inconnu (l'opérateur de comparaison est '==')"<<endl;
						exit(-1);
				}
				cout << "\tpush $0"<<endl;
				cout << "\tjmp SUITE"<<TagNumber<<endl;
				cout << "VRAI"<<TagNumber<<":"<<endl;
				cout << "\tpush $0xFFFFFFFFFFFFFFFF"<<endl;	
				cout << "SUITE"<<TagNumber<<":"<<endl;
				return (BOOL);
			}
		}
		else {
			cerr << "Erreur ligne n°" << lexer->lineno() <<" : Opération relationelle impossible entre valeur numérique et booléen"<<endl;
			exit(-1);
		}
	}
	else
		return (t);
}

// AssignementStatement := Ident ":=" ( Expression | '"' Char '"' | "VRAI" | "FAUX" )
void AssignementStatement(void){
	string variable;
	TYPE t;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if(current==CHARCONST){
		t=Char();
	}
	else if(current==KEYWORD){
		t=Boolean();
	}
	else {
		t=Expression();
	}
	if(DeclaredVariables[variable]==t)
		cout << "\tpop "<<variable<<endl;
	else {
		cerr << "Erreur ligne n°" << lexer->lineno() <<" : Valeur de type "<<TypeToString(t)<<" ne peut être assignée à une variable de type "<<TypeToString(DeclaredVariables[variable])<<endl;
		exit(-1);
	}
}

void Statement(void);

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu (ou caractère ';' manquant à la fin de la ligne précédente)");
	current=(TOKEN) lexer->yylex();
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement (void){
	int tag=++TagNumber;
	TYPE t;
	if(current==KEYWORD && strcmp(lexer->YYText(),"IF")==0){
		cout<<"IF"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		t=Expression();
		if (t==BOOL) {
			cout<<"\tpop %rax"<<endl;
			cout<<"\tcmpq $0, %rax"<<endl;
			cout<<"\tje ELSE"<<tag<<endl;
			if(current==KEYWORD && strcmp(lexer->YYText(),"THEN")==0)
			{
				cout<<"THEN"<<tag<<":"<<endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				cout<<"jmp ENDIF"<<tag<<endl;
				cout<<"ELSE"<<tag<<":"<<endl;
				if(current==KEYWORD && strcmp(lexer->YYText(),"ELSE")==0)
				{
					current=(TOKEN) lexer->yylex();
					Statement();
					cout<<"jmp ENDIF"<<tag<<endl;
				}
				cout<<"ENDIF"<<tag<<":"<<endl;
			}
			else
				Error("THEN attendu");
		}
		else
			Error("conflit de type");
	}
	else
		Error("IF attendu");
}

// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement (void){
	int tag=++TagNumber;
	TYPE t;
	if(current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0)
	{
		cout<<"WHILE"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		t=Expression();
		if (t==BOOL){
			cout<<"\tpop %rax"<<endl;
			cout<<"\tcmpq $0, %rax"<<endl;
			cout<<"\tje ENDWHILE"<<tag<<endl;
			cout<<"DO"<<tag<<":"<<endl;
			if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				cout<<"\tjmp WHILE"<<tag<<endl;
				cout<<"ENDWHILE"<<tag<<":"<<endl;
			}
			else 
				Error("DO attendu");
		}
		else {
			cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le résultat de l'expression de la fonction WHILE doit être un BOOL"<<endl;
			exit(-1);
		}
	}
	else
		Error("WHILE attendu");
}

// ForStatement := "FOR" AssignementStatement ( "TO" | "DOWNTO" ) Expression ["STEP" Expression] "DO" Statement
void ForStatement (void){
	int tag=++TagNumber;
	TYPE t,step;
	if(current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0)
	{
		cout<<"FOR"<<tag<<":"<<endl;
		cout<<"\tpush $1"<<endl;
		cout<<"\tpop forstep"<<endl;
		current=(TOKEN) lexer->yylex();
		string variable=lexer->YYText();
		t=DeclaredVariables[variable];
		if(t!=INT){
			cerr << "Erreur ligne n°" << lexer->lineno() <<" : La variable de comptage de la fonction FOR doit être de type INT"<<endl;
			exit(-1);
		}
		AssignementStatement();
		if(current==KEYWORD && strcmp(lexer->YYText(),"TO")==0){
			cout<<"TO"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			t=Expression();
			if(t==INT){
				cout<<"\tpop %rax"<<endl;
				cout<<"\tpush "<<variable<<endl;
				cout<<"\tpop %rbx"<<endl;
				cout<<"\tcmpq %rax, %rbx"<<endl;
				cout<<"\tja ENDFOR"<<tag<<endl;
				if(current==KEYWORD && strcmp(lexer->YYText(),"STEP")==0){
					current=(TOKEN) lexer->yylex();
					step=Expression();
					if(step==INT)
						cout<<"\tpop forstep"<<endl;
					else {
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le pas de la fonction FOR doit être de une valeur de type INT"<<endl;
						exit(-1);
					}
				}
				if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0)
				{
					cout<<"DO"<<tag<<":"<<endl;
					current=(TOKEN) lexer->yylex();
					Statement();
					cout<<"\tpush forstep"<<endl;
					cout<<"\tpop %rax"<<endl;
					cout<<"\taddq %rax, "<<variable<<endl;
					cout<<"\tjmp TO"<<tag<<endl;
					cout<<"ENDFOR"<<tag<<":"<<endl;
				}
				else
					Error("mot clé 'DO' ou 'STEP' attendu");
			}
			else {
				cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le résultat de l'expression de la fonction FOR doit être un INT"<<endl;
				exit(-1);
			}
		}
		else if(current==KEYWORD && strcmp(lexer->YYText(),"DOWNTO")==0){
			cout<<"DOWNTO"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			t=Expression();
			if(t==INT){
				cout<<"\tpop %rax"<<endl;
				cout<<"\tpush "<<variable<<endl;
				cout<<"\tpop %rbx"<<endl;
				cout<<"\tcmpq %rax, %rbx"<<endl;
				cout<<"\tjz ENDFOR"<<tag<<endl;
				cout<<"\tjb ENDFOR"<<tag<<endl;
				if(current==KEYWORD && strcmp(lexer->YYText(),"STEP")==0){
					current=(TOKEN) lexer->yylex();
					step=Expression();
					if(step==INT)
						cout<<"\tpop forstep"<<endl;
					else {
						cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le pas de la fonction FOR doit être de une valeur de type INT"<<endl;
						exit(-1);
					}
				}
				if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0)
				{
					cout<<"DO"<<tag<<":"<<endl;
					current=(TOKEN) lexer->yylex();
					Statement();
					cout<<"\tpush forstep"<<endl;
					cout<<"\tpop %rax"<<endl;
					cout<<"\tcmpq %rax, "<<variable<<endl;
					cout<<"\tjb ENDFOR"<<tag<<endl;
					cout<<"\tsubq %rax, "<<variable<<endl;
					cout<<"\tjmp DOWNTO"<<tag<<endl;
					cout<<"ENDFOR"<<tag<<":"<<endl;
				}
				else
					Error("mot clé 'DO' ou 'STEP' attendu");
			}
			else {
				cerr << "Erreur ligne n°" << lexer->lineno() <<" : Le résultat de l'expression de la fonction FOR doit être un INT"<<endl;
				exit(-1);
			}
		}
		else
			Error("mot clé 'TO' ou 'DOWNTO' attendu");
		
	}
	else
		Error("FOR attendu");
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement (void){
	int tag=++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0)
	{
		cout<<"BEGIN"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		while(current==SEMICOLON){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(current!=KEYWORD && strcmp(lexer->YYText(),"END")!=0)
			Error("END attendu");
		cout<<"END"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
	}
}

//Display := "DISPLAY" Expression
void Display(){
	int tag=++TagNumber;
	TYPE t;
	if(current==KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0){
		cout<<"#DISPLAY"<<endl;
		current=(TOKEN) lexer->yylex();
		t=Expression();
		switch(t){
			case INT:
				cout<<"\tpop %rsi"<<endl;							
				cout<<"\tmovq $FormatString1, %rdi"<<endl;
				cout<<"\tmovb $0, %al"<<endl;
				cout<<"\tsubq $8, %rsp"<<endl;
				cout<<"\tcall printf"<<endl;
				cout<<"\taddq $8, %rsp"<<endl;
				cout<<"#DISPLAYED"<<endl;
				break;
			case BOOL:
				cout<<"\tpop %rsi"<<endl;
				cout<<"\tcmpq $0, %rsi"<<endl;
				cout<<"\tje FALSE"<<tag<<endl;
				cout<<"\tmovq $1, %rsi"<<endl;
				cout<<"FALSE"<<tag<<":"<<endl;
				cout<<"\tmovq $FormatString1, %rdi"<<endl;
				cout<<"\tmovb $0, %al"<<endl;
				cout<<"\tsubq $8, %rsp"<<endl;
				cout<<"\tcall printf"<<endl;
				cout<<"\taddq $8, %rsp"<<endl;
				cout<<"#DISPLAYED"<<endl;
				break;
			case CHAR:
				cout<<"\tpop %rsi"<<endl;							
				cout<<"\tmovq $FormatString3, %rdi"<<endl;
				cout<<"\tmovb $0, %al"<<endl;
				cout<<"\tsubq $8, %rsp"<<endl;
				cout<<"\tcall printf"<<endl;
				cout<<"\taddq $8, %rsp"<<endl;
				cout<<"#DISPLAYED"<<endl;
				break;
			case DOUBLE:
				cout<<"\tmovsd (%rsp), %xmm0"<<endl;
				cout<<"\taddq $8, %rsp"<<endl;						
				cout<<"\tmovq $FormatString2, %rdi"<<endl;
				cout<<"\tmovb $1, %al"<<endl;
				cout<<"\tsubq $8, %rsp"<<endl;
				cout<<"\tcall printf"<<endl;
				cout<<"\taddq $8, %rsp"<<endl;
				cout<<"#DISPLAYED"<<endl;
				break;
		}
	}
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void){
	if (current==ID)
		AssignementStatement();
	else if (current==KEYWORD && strcmp(lexer->YYText(),"IF")==0)
		IfStatement();
	else if (current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0)
		WhileStatement();
	else if (current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0)
		ForStatement();
	else if (current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0)
		BlockStatement();
	else if (current==KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0)
		Display();
}		

// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t#used by printf to display 64-bit unsigned integers"<<endl;
	cout << "FormatString2:\t.string \"%f\\n\"\t\t#used by printf to display 64-bit unsigned integers"<<endl;
	cout << "FormatString3:\t.string \"%c\\n\"\t\t#used by printf to display 64-bit unsigned integers"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}
